const FIRST_NAME = "Ioana-Andreea";
const LAST_NAME = "Berdei";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function initCaching() { 
  var cache = {};
  cache.pageAccessCounter = function(section) {
    if (typeof section === "string") section = section.toLowerCase();
    if (!section) {
      if (cache["home"]) cache["home"]++;
      else cache["home"] = 1;
    }
    if (cache[section]) cache[section]++;
    else cache[section] = 1;
  };
  cache.getCache = function() {
    return cache;
  };
  return cache;
}

module.exports = {
  FIRST_NAME,
  LAST_NAME,
  GRUPA,
  initCaching
};
